public class Main {
    public static void main(String[] args) {
        Thing2 insan = new Thing2("Elmin", "Mugalov");
        System.out.println(insan.name + " " + insan.surname);

        Thing3 insanklon = new Thing3("Elmin", "Mugalov Kuon");
        System.out.println(insanklon.name + " " + insanklon.surname);
    }
}